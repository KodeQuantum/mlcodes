echo "python version"
python --version
echo ; echo ;

echo "pip3 version"
pip3 --version
echo ; echo ;

echo "pip3 packages"
ll="tensorflow,keras,pandas,sklearn,scikit-optimize,matplotlib,sns,numpy"
echo "looking for $ll"
pip3 list | grep -i "tensorflow\|keras\|pandas\|sklearn\|scikit-optimize\|matplotlib\|sns\|numpy"
echo ; echo ;


