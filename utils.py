import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import tensorflow as tf
from sklearn.model_selection import train_test_split 


frn = lambda x : tf.experimental.numpy.random.randint(x[0], high=x[1], size=x[2], dtype=tf.experimental.numpy.int64)


rtn = lambda x : tf.random.uniform(shape=x)


def lshapes(tl):
    for e in range(0,len(tl)):
        print(e, ':', tl[e].shape)


def fpm(train) :

    return tf.keras.utils.plot_model(
    train, to_file='model.png', show_shapes=True, show_dtype=False,
    show_layer_names=True, rankdir='TB', expand_nested=False, dpi=96,
    layer_range=None)



# alias for test train split
# time series shuffle
# {dx, dy} : datafram from gensetsall
def tts(dx, dy, p=0.3):
    print("meaning of symbols")
    print('x:fv, y:target, r:train, s:test')
    xr, xs, yr, ys = train_test_split(dx, dy, test_size=p, random_state=42)
    return xr, xs, yr, ys


# fig size
def fs(e):
    # set
    if e==1 : plt.figure(figsize=(15,12))
        
    # reset
    plt.figure(figsize=(6,4))



# normalization by max
# returns if max less than tolerance
# same as data1()
def data2(mt=1e-4):

    plt.rcParams['figure.figsize']
    df = pd.read_csv('ipf.csv')
    print("no of rows: ", df.shape)
    co = {'date':'d',"machine":'m', "event":'e'}

    bs = 'feature'
    rd = {} # dict for rename
    nl = [] # new label

    for e in range(1,10):
        bo = '{}{}'.format(bs,e) # old 
        bn = '{}'.format(e) # new
        rd[bo] = bn
        nl.append(bn)
        
    rd, nl

    nc = {'date':'d',"machine":'m', "event":'e', **rd}
    df = df.rename(columns=nc)

    print(df.columns)

    df2 = df.groupby(['d'], as_index=False).sum()

    print('after date aggregation per day: ', df2.shape)

    df2.groupby(['e']).count()

    df2[ df2['e'] > 2 ]

    df3 = df2[ df2['e'] < 2 ]

    df3['e'].unique()

    df3.groupby(['e']).count()
    print('after event aggregation : ', df3.shape)

    df4 = df3.copy()

    fc = ['{}'.format(e) for e in range(1,10)]
    fc

    for e in fc : 
        x = df3[e].max()
        print('fv:{}, max:{}'.format(e,x))
        if x <= mt :
            return 

    for e in fc : 
        #l = 'f{}'.format(e)
        df4[e] = df3[e]/df3[e].max()

    df = df4.copy()

    print('SUCCESS')
    return df



def data1():

    plt.rcParams['figure.figsize']
    df = pd.read_csv('ipf.csv')
    print("no of rows: ", df.shape)
    co = {'date':'d',"machine":'m', "event":'e'}

    bs = 'feature'
    rd = {} # dict for rename
    nl = [] # new label

    for e in range(1,10):
        bo = '{}{}'.format(bs,e) # old 
        bn = '{}'.format(e) # new
        rd[bo] = bn
        nl.append(bn)
        
    rd, nl

    nc = {'date':'d',"machine":'m', "event":'e', **rd}
    df = df.rename(columns=nc)

    print(df.columns)

    df2 = df.groupby(['d'], as_index=False).sum()

    print('after date aggregation per day: ', df2.shape)

    df2.groupby(['e']).count()

    df2[ df2['e'] > 2 ]

    df3 = df2[ df2['e'] < 2 ]

    df3['e'].unique()

    df3.groupby(['e']).count()
    print('after event aggregation : ', df3.shape)

    df4 = df3.copy()

    fc = ['{}'.format(e) for e in range(1,10)]
    fc

    for e in fc : 
        #l = 'f{}'.format(e)
        df4[e] = df3[e]/df3[e].sum()

    df = df4.copy()

    return df



# return cols with integer strings
def allcols():
    return [str(e) for e in range(1,10)]

# generate serially time series sets 
# on already filtered information
# s : split size, or time slice
# outputs : dx : source, dy: target, dg : {dx, dy} together
def gensetsall(s=10):

    dx = []
    dy = []
    dg = []

    df = data2()

    # columns
    fc = [str(e) for e in range(1,10)]

    n = df.shape[0]
    p = int(n/s)

    for e in range(1,p+1):

        kx = e*s
        kn = (e-1)*s

        dt = df[kn:kx]
        dg.append(dt)

        dy.append(dt['e'])
        dx.append(dt[fc])

    # dx = target vector, e
    # dy = all columns, labelled as integers
    return dx, dy, dg




# generate serially time series sets
# on already filtered information
# s : split size, or time slice
# outputs : dx : source, dy: target, dg : {dx, dy} together
def gensetsall1(s=10):

    dx = []
    dy = []

    df = data2()
    de = pd.get_dummies(df['e'],prefix='e')

    # columns
    fc = [str(e) for e in range(1,10)]

    n = df.shape[0]
    p = int(n/s)

    for e in range(1,p+1):

        kx = e*s
        kn = (e-1)*s

        dt = df[kn:kx]
        ds = de[kn:kx]

        dy.append(ds)
        dx.append(dt[fc])

    # dx = target vector, e
    # dy = all columns, labelled as integers
    return dx, dy
